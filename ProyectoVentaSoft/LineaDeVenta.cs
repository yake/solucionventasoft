﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoVentaSoft
{
    public class LineaDeVenta
    {
        private int cantidadVendida;
        private double precioDeVenta;
        private Producto producto;

        public int CantidadVendida { get => cantidadVendida; set => cantidadVendida = value; }
        public double PrecioDeVenta { get => precioDeVenta; set => precioDeVenta = value; }
        public Producto Producto { get => producto; set => producto = value; }

        public double calcularSubTotal()
        {
            return (cantidadVendida * Producto.PrecioDeCompra);
        }

        public Boolean esValida()
        {
            return ((Producto.StockActual - cantidadVendida) >= Producto.StockMinimo);
        }
    }
}
