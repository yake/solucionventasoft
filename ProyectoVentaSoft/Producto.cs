﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoVentaSoft
{
    public class Producto
    {
        private string codigo;
        private string nombre;
        private string tipoUnidad;
        private double precioDeCompra;
        private int stockActual;
        private int stockMinimo;
        private int stockMaximo;

        public string Codigo { get => codigo; set => codigo = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string TipoUnidad { get => tipoUnidad; set => tipoUnidad = value; }
        public double PrecioDeCompra { get => precioDeCompra; set => precioDeCompra = value; }
        public int StockActual { get => stockActual; set => stockActual = value; }
        public int StockMinimo { get => stockMinimo; set => stockMinimo = value; }
        public int StockMaximo { get => stockMaximo; set => stockMaximo = value; }


        public double calcularPrecioDeVenta()
        {
            return (precioDeCompra + (precioDeCompra*0.2));
        }

        public Boolean esStockMinimoValido()
        {
            return (stockMinimo < stockMaximo);
        }
        
        public Boolean esStockMaximoValido()
        {
            return (stockMaximo > stockMinimo);
        }

        public Boolean esStockActualValido()
        {
            return (stockActual >= stockMinimo && stockActual <= stockMaximo);
        }
    }
}
