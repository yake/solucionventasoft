﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoVentaSoft
{
    public class Cliente
    {
        private string dni;
        private string nombre;

        public Cliente(String nombre, String dni)
        {
            this.Nombre = nombre;
            this.Dni = dni;
        }

        public string Dni { get => dni; set => dni = value; }
        public string Nombre { get => nombre; set => nombre = value; }
    }
}
