﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoVentaSoft
{
    public class Venta
    {
        private string numero;
        private string fecha;
        private Cliente cliente;
        private List<LineaDeVenta> lineasDeVentas = new List<LineaDeVenta>();

        public string Numero { get => numero; set => numero = value; }
        public string Fecha { get => fecha; set => fecha = value; }
        public Cliente Cliente { get => cliente; set => cliente = value; }
        public List<LineaDeVenta> LineasDeVentas { get => lineasDeVentas; set => lineasDeVentas = value; }

        public bool agregarLineaDeVenta(int cantidad, Producto producto)
        {
            LineaDeVenta lineaDeVenta = new LineaDeVenta();
            lineaDeVenta.CantidadVendida = cantidad;
            lineaDeVenta.PrecioDeVenta = producto.PrecioDeCompra;
            lineaDeVenta.Producto = producto;

            if (lineaDeVenta.esValida())
            {
                lineasDeVentas.Add(lineaDeVenta);
                return true;

            }

            return false;
                        
        }

        public double calcularTotal()
        {
            double total = 0;
            foreach(LineaDeVenta lineaDeVenta in lineasDeVentas)
            {
                total += lineaDeVenta.calcularSubTotal();
            }

            return total;
        }

        public double calcularDescuento()
        {
            double totalCalculado = calcularTotal();
            double descuento = 0;

            if(totalCalculado > 100)
            {
                descuento = 0.1 * totalCalculado;
            }

            return descuento;
        }

        public double calcularPago()
        {
            return calcularTotal() - calcularDescuento();
        }

        public bool esMontoPagoValido(double montoPagado)
        {
            return montoPagado >= calcularPago();
        }

        public double calcularVuelto(double montoPagado)
        {
            double pagoCalculado = calcularPago();
            double vuelto = 0;

            if (montoPagado > pagoCalculado)
            {
                vuelto = montoPagado - pagoCalculado;
            }
            
            return vuelto;
        }
    }
}
