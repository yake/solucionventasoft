﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProyectoVentaSoft;

namespace ProyectoVentaSoftTest {
    [TestClass]
    public class LineaDeVentaTest {
        private LineaDeVenta crearLineaDeVenta() {
            Producto producto = new Producto();
            producto.PrecioDeCompra = 8;
            producto.StockActual = 2;
            producto.StockMinimo = 0;
            producto.StockMaximo = 4;

            LineaDeVenta lineaDeVenta = new LineaDeVenta();
            lineaDeVenta.CantidadVendida = 2;
            lineaDeVenta.Producto = producto;

            return lineaDeVenta;
        }
        [TestMethod]
        public void calcularSubTotalTestMethod() {
            LineaDeVenta lineaDeVenta = crearLineaDeVenta();

            Assert.AreEqual(16,
                            lineaDeVenta.calcularSubTotal());
        }
        [TestMethod]
        public void esValidaTestMethod() {
            LineaDeVenta lineaDeVenta = crearLineaDeVenta();

            Assert.IsTrue(lineaDeVenta.esValida());
        }
    }
}
