﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProyectoVentaSoft;

namespace ProyectoVentaSoftTest {
    [TestClass]
    public class ProductoTest {
        private Producto crearProducto() {
            Producto producto = new Producto();
            producto.PrecioDeCompra = 8;
            producto.StockActual = 2;
            producto.StockMinimo = 0;
            producto.StockMaximo = 4;

            return producto;
        }
        [TestMethod]
        public void calcularPrecioDeVentaTestMethod() {
            Producto producto = crearProducto();

            Assert.AreEqual(producto.PrecioDeCompra * 1.2,
                            producto.calcularPrecioDeVenta());
        }
        [TestMethod]
        public void esStockMinimoValidoTestMethod() {
            Producto producto = crearProducto();

            Assert.IsTrue(producto.esStockMinimoValido());
        }
        [TestMethod]
        public void esStockMaximoValidoTestMethod() {
            Producto producto = crearProducto();

            Assert.IsTrue(producto.esStockMaximoValido());
        }

        [TestMethod]
        public void esStockActualValidoTestMethod() {
            Producto producto = crearProducto();

            Assert.IsTrue(producto.esStockActualValido());
        }
    }
}
