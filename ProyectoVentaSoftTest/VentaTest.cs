﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProyectoVentaSoft;

namespace ProyectoVentaSoftTest {

    [TestClass]
    public class VentaTest {
        private Venta crearVenta() {
            Producto producto;
            Venta venta = new Venta();

            producto = new Producto();
            producto.PrecioDeCompra = 5;
            producto.StockActual = 2;
            producto.StockMinimo = 0;
            producto.StockMaximo = 4;
            venta.agregarLineaDeVenta(2, producto);

            producto = new Producto();
            producto.PrecioDeCompra = 50;
            producto.StockActual = 2;
            producto.StockMinimo = 0;
            producto.StockMaximo = 2;
            venta.agregarLineaDeVenta(1, producto);

            producto = new Producto();
            producto.PrecioDeCompra = 10;
            producto.StockActual = 5;
            producto.StockMinimo = 0;
            producto.StockMaximo = 5;
            venta.agregarLineaDeVenta(5, producto);

            return venta;
        }

        [TestMethod]
        public void agregarLineaDeVenta() {
            Venta venta = new Venta();
            Producto producto = new Producto();

            producto.PrecioDeCompra = 5;
            producto.StockActual = 2;
            producto.StockMinimo = 0;
            producto.StockMaximo = 4;

            Assert.IsTrue(venta.agregarLineaDeVenta(2,producto));
        }
        [TestMethod]
        public void calcularTotalTestMethod() {
            Venta venta = crearVenta();

            Assert.AreEqual(110,
                            venta.calcularTotal());
        }
        [TestMethod]
        public void calcularDescuentoTestMethod() {
            Venta venta = crearVenta();

            Assert.AreEqual(11,
                            venta.calcularDescuento());
        }
        [TestMethod]
        public void calcularPagoTestMethod() {
            Venta venta = crearVenta();

            Assert.AreEqual(99,
                            venta.calcularPago());
        }
        [TestMethod]
        public void esMontoPagoValidoTestMethod() {
            Venta venta = crearVenta();

            Assert.IsTrue(venta.esMontoPagoValido(99));
        }
        [TestMethod]
        public void calcularVueltoTestMethod() {
            Venta venta = crearVenta();

            Assert.AreEqual(1,
                            venta.calcularVuelto(100));
        }
    }
}
